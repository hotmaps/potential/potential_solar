[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687554.svg)](https://doi.org/10.5281/zenodo.4687554)

# Solar ENERGY POTENTIAL

## Repository structure

```
nuts3_dataset\datapackage.json           -- Datapackage JSON file with themain meta-data
nuts3_dataset\output_potential.csv       -- CSV data with the NUTS3 data set
raster       
```

## Description of the task

The present task provides data with following characteristics:

<table>
  <tr>
    <td> </td>
    <td>Spatial resolution</td>
    <td>Temporal resolution</td>
  </tr>
  <tr>
    <td>Solar potential</td>
    <td>EU28</td>
    <td>yearly</td>
  </tr>
</table>

**Table 1.** Characteristics of data provided within Task 2.6 Renewable energy sources data collection and potential review.

In this task, we collected and re-elaborated data on energy potential of renewable sources at national level, in order to build datasets for all EU28 countries at NUTS3 level. We considered the following renewable sources: biomass, waste and wastewater, shallow geothermal, wind, and solar energy. These data will be used in the toolbox to map the sources of renewable thermal energy across the EU28 and support energy planning and policy.

### Solar energy

Data on annual global radiation on globally inclined surfaces in kWh/m2 were retrieved from the [PVGIS](http://re.jrc.ec.europa.eu/pvgis/apps4/pvest.php#) as a 1km x 1km raster layer and clipped by considering the building footprint with a resolution of 100m x 100m from [Copernicus Services](http://land.copernicus.eu/pan-european/GHSL/european-settlement-map/EU%20GHSL%202014). The data were used as indicator to define patterns in Chiara Scaramuzzino, Giulia Garegnani, Pietro Zambelli, Integrated approach for the identification of spatial patterns related to renewable energy potential in European territories, Renewable and Sustainable Energy Reviews, Volume 101, 2019, Pages 1-13, ISSN 1364-0321,
[https://doi.org/10.1016/j.rser.2018.10.024](https://doi.org/10.1016/j.rser.2018.10.024).



#### Methodology

The raster file mapping the solar radiation was imported in Grass GIS and crossed with the building footprint.

## Limitations of data

The data here calculated are estimations of the energy potential from renewable energy sources. The hypotheses we made when deciding what data to consider, when re-elaborating the data at more aggregated territorial levels and finally when deciding how to convey the results, can influence the results.

In some cases, we underestimated the actual potential (biomass), by downscaling the available resource for sustainability reasons, in others, we overestimated the potential (wind, solar) due to our assumption of using all available areas, according only to some GIS sustainable criteria, where energy generation is feasible without considering economic profitability.

The potentials here reported **do not account for any type of energy conversion**: when estimating the actual potential, the user will need to choose the technology through which the potential can be exploited (for example COP for the wastewater treatment plant or the efficiency for solar thermal, photovoltaic and wind).

For these reasons, the data must be considered **as indicators, rather than absolute figures** representing the actual energy potential of renewable sources in a territory.

## How to cite

@article{SCARAMUZZINO20191,
title = "Integrated approach for the identification of spatial patterns related to renewable energy potential in European territories",
journal = "Renewable and Sustainable Energy Reviews",
volume = "101",
pages = "1 - 13",
year = "2019",
issn = "1364-0321",
doi = "https://doi.org/10.1016/j.rser.2018.10.024",
url = "http://www.sciencedirect.com/science/article/pii/S1364032118307275",
author = "Chiara Scaramuzzino and Giulia Garegnani and Pietro Zambelli",
keywords = "Renewable energy sources, Energy planning, Energy policy, EU28",
abstract = "The study presents an effort to classify the territories of a specific area, according to similarities in the estimated potential of their renewable sources, considering also their economic and sociodemographic structure and their geographic features. Specifically, the paper focuses on the area of EU28 and Switzerland and uses as basis for the analysis, data estimating the potential of renewable energy sources collected and elaborated in the framework of the project HotMaps (Horizon 2020). The method used to group the territorial units is cluster analysis, and specifically the k-means algorithm. The data present some interesting patterns and the territories of EU28 and Switzerland at NUTS3 level are classified into 17 clusters. The analysis shows the presence of heterogeneity within national borders and among territories comprised in the macro regions target of specific EU programmes, specifically the Adriatic-Ionian region, the Alpine region, the Baltic Sea region and the Danube region. The results of this research are meant to be used by European policy makers in developing more focused transnational renewable energy policies and strategies."
}


## Authors

Chiara Scaramuzzino<sup>*</sup>,
Pietro Zambelli<sup>*</sup>,
Giulia Garegnani<sup>*</sup> 

<sup>*</sup> Eurac Research 

Institute for Renewable Energy
VoltaStraße/Via Via A. Volta 13/A
39100 Bozen/Bolzano

## License

Copyright © 2016-2018: Giulia Garegnani <giulia.garegnani@eurac.edu>,  Pietro Zambelli <pietro.zambelli@eurac.edu>
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0
License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.
